﻿using MediaBalans.Application.Abstractions.Services;
using MediaBalans.Application.Abstractions.Services.Authentication;
using MediaBalans.Application.Repositories.Category;
using MediaBalans.Application.Repositories.File;
using MediaBalans.Application.Repositories.Product;
using MediaBalans.Application.Repositories.ProductImageFile;
using MediaBalans.Domain.Entities.Identity;
using MediaBalans.Persistence.Contexts;
using MediaBalans.Persistence.Repositories.Category;
using MediaBalans.Persistence.Repositories.File;
using MediaBalans.Persistence.Repositories.Product;
using MediaBalans.Persistence.Repositories.ProductImageFile;
using MediaBalans.Persistence.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence
{
    public static class ServiceRegistration
    {
        public static void AddPersistenceServices(this IServiceCollection services)
        {
            services.AddDbContext<MediaBalansDbContext>(options => options.UseNpgsql(Configuration.ConnectionString));
            services.AddIdentity<AppUser, AppRole>(options =>
            {
                options.Password.RequiredLength = 3;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
            }).AddEntityFrameworkStores<MediaBalansDbContext>();

            services.AddScoped<IProductReadRepository, ProductReadRepository>();
            services.AddScoped<IProductWriteRepository, ProductWriteRepository>();
            services.AddScoped<IFileReadRepository, FileReadRepository>();
            services.AddScoped<IFileWriteRepository, FileWriteRepository>();
            services.AddScoped<IProductImageFileReadRepository, ProductImageFileReadRepository>();
            services.AddScoped<IProductImageFileWriteRepository, ProductImageFileWriteRepository>();
            services.AddScoped<ICategoryReadRepository, CategoryReadRepository>();
            services.AddScoped<ICategoryWriteRepository, CategoryWriteRepository>();


            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IInternalAuthentication, AuthService>();
        }
    }
}
