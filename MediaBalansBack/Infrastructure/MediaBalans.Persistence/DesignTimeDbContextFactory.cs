﻿using MediaBalans.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<MediaBalansDbContext>
    {
        public MediaBalansDbContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<MediaBalansDbContext> dbContextOptionsBuilder = new();
            dbContextOptionsBuilder.UseNpgsql(Configuration.ConnectionString);
            return new(dbContextOptionsBuilder.Options);
        }
    }
}
