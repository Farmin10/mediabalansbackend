﻿using MediaBalans.Application.Repositories.Category;
using MediaBalans.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence.Repositories.Category
{
    public class CategoryWriteRepository : WriteRepository<MediaBalans.Domain.Entities.Category>, ICategoryWriteRepository
    {
        public CategoryWriteRepository(MediaBalansDbContext context) : base(context)
        {
        }
    }
}
