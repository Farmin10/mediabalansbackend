﻿using MediaBalans.Application.Repositories.File;
using MediaBalans.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence.Repositories.File
{
    public class FileReadRepository : ReadRepository<MediaBalans.Domain.Entities.File>, IFileReadRepository
    {
        public FileReadRepository(MediaBalansDbContext context) : base(context)
        {
        }
    }
}
