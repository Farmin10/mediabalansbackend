﻿using MediaBalans.Application.Repositories.ProductImageFile;
using MediaBalans.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence.Repositories.ProductImageFile
{
    public class ProductImageFileReadRepository : ReadRepository<MediaBalans.Domain.Entities.ProductImageFile>, IProductImageFileReadRepository
    {
        public ProductImageFileReadRepository(MediaBalansDbContext context) : base(context)
        {
        }
    }
}
