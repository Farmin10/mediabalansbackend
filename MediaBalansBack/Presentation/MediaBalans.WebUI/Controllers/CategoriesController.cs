﻿using MediaBalans.Application.Features.Commands.Category.CreateCategory;
using MediaBalans.Application.Features.Commands.Category.UpdateCategory;
using MediaBalans.Application.Features.Queries.Category.GetAllCategory;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace MediaBalans.WebUI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ProductsController> _logger;

        public CategoriesController(IMediator mediator, ILogger<ProductsController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost("CreateCategory")]
        [Authorize(AuthenticationSchemes = "Admin")]
        public async Task<IActionResult> CreateCategory(CreateCategoryCommandRequest createCategoryCommandRequest)
        {
            CreateCategoryCommandResponse response = await _mediator.Send(createCategoryCommandRequest);
            return StatusCode((int)HttpStatusCode.Created);
        }


        [HttpPut("UpdateCategory")]
        [Authorize(AuthenticationSchemes = "Admin")]
        public async Task<IActionResult> UpdateCategory([FromBody] UpdateCategoryCommandRequest updateCategoryCommandRequest)
        {
            UpdateCategoryCommandResponse response = await _mediator.Send(updateCategoryCommandRequest);
            return Ok(response);
        }



        [HttpGet("GetCategories")]
        public async Task<IActionResult> GetCategories([FromQuery] GetAllCategoryQueryRequest getAllCategoryQueryRequest)
        {
            GetAllCategoryQueryResponse response = await _mediator.Send(getAllCategoryQueryRequest);
            return Ok(response);
        }
    }
}
