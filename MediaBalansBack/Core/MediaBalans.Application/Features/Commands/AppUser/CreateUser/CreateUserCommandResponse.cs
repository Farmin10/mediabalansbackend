﻿namespace MediaBalans.Application.Features.Commands.AppUser.CreateUser
{
    public class CreateUserCommandResponse
    {
        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public string Id { get; set; }
    }
}
