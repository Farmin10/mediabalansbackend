﻿using FluentValidation;
using MediaBalans.Application.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Application.Validators.Products
{
    public class CreateProductValidator : AbstractValidator<VM_Create_Product>
    {
        public CreateProductValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .NotNull()
                    .WithMessage("Please fill product name.")
                .MaximumLength(150)
                .MinimumLength(5)
                    .WithMessage("Please fill product name between 5 and 150 caracter");

            RuleFor(p => p.Stock)
                .NotEmpty()
                .NotNull()
                    .WithMessage("Please fill stock.")
                .Must(s => s >= 0)
                    .WithMessage("Stock cannot be negative!");

            RuleFor(p => p.Price)
                .NotEmpty()
                .NotNull()
                    .WithMessage("Please fill price.")
                .Must(s => s >= 0)
                    .WithMessage("Price cannot be negative!");
        }
    }
}
