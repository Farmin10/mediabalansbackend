﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Application.Repositories.File
{
    public interface IFileWriteRepository : IWriteRepository<MediaBalans.Domain.Entities.File>
    {
    }
}
