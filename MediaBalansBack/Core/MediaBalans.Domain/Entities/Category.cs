﻿using MediaBalans.Domain.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Domain.Entities
{
    public class Category:BaseEntity
    {
        public string  CategoryName { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
