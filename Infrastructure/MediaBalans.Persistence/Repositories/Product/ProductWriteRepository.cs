﻿using MediaBalans.Application.Repositories.Product;
using MediaBalans.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence.Repositories.Product
{
    public class ProductWriteRepository : WriteRepository<MediaBalans.Domain.Entities.Product>, IProductWriteRepository
    {
        public ProductWriteRepository(MediaBalansDbContext context) : base(context)
        {
        }
    }
}
