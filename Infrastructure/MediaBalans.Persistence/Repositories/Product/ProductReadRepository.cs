﻿using MediaBalans.Application.Repositories.Product;
using MediaBalans.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence.Repositories.Product
{
    public class ProductReadRepository : ReadRepository<MediaBalans.Domain.Entities.Product>, IProductReadRepository
    {
        public ProductReadRepository(MediaBalansDbContext context) : base(context)
        {
        }
    }
}
