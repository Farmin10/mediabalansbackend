﻿using MediaBalans.Application.Repositories.File;
using MediaBalans.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence.Repositories.File
{
    public class FileWriteRepository : WriteRepository<MediaBalans.Domain.Entities.File>, IFileWriteRepository
    {
        public FileWriteRepository(MediaBalansDbContext context) : base(context)
        {
        }
    }
}
