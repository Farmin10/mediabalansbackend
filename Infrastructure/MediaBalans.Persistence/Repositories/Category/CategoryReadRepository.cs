﻿using MediaBalans.Application.Repositories.Category;
using MediaBalans.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Persistence.Repositories.Category
{
    public class CategoryReadRepository : ReadRepository<MediaBalans.Domain.Entities.Category>, ICategoryReadRepository
    {
        public CategoryReadRepository(MediaBalansDbContext context) : base(context)
        {
        }
    }
}
