﻿using FluentValidation;
using MediaBalans.Application.ViewModels.Categories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Application.Validators.Categories
{
    public class CreateCategoryValidator : AbstractValidator<VM_Create_Category>
    {
        public CreateCategoryValidator()
        {
            RuleFor(p => p.CategoryName)
               .NotEmpty()
               .NotNull()
                   .WithMessage("Please fill Category name.")
               .MaximumLength(150)
               .MinimumLength(5)
                   .WithMessage("Please fill Category name between 5 and 150 caracter");
        }
    }
}
