﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Application.Repositories.Product
{
    public interface IProductWriteRepository : IWriteRepository<MediaBalans.Domain.Entities.Product>
    {
    }
}
