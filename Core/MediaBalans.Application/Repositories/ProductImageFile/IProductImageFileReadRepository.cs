﻿using MediaBalans.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Application.Repositories.ProductImageFile
{
    public interface IProductImageFileReadRepository : IReadRepository<MediaBalans.Domain.Entities.ProductImageFile>
    {
    }
}
