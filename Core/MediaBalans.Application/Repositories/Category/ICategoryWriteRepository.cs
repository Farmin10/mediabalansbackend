﻿using MediaBalans.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Application.Repositories.Category
{
    public interface ICategoryWriteRepository : IWriteRepository<MediaBalans.Domain.Entities.Category>
    {
    }
}
