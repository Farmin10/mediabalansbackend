﻿namespace MediaBalans.Application.Features.Commands.Product.CreateProduct
{
    public class CreateProductCommandResponse
    {
        public Guid Id { get; set; }
    }
}
