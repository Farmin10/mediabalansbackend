﻿using MediaBalans.Application.Repositories.Product;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MediaBalans.Application.Features.Queries.ProductImageFile
{
    public class GetProductImagesQueryHandler : IRequestHandler<GetProductImagesQueryRequest, List<GetProductImagesQueryResponse>>
    {
        readonly IProductReadRepository _productReadRepository;
        readonly IConfiguration configuration;
        private readonly IWebHostEnvironment _webHostEnvironmentenv;

        public GetProductImagesQueryHandler(IProductReadRepository productReadRepository, IConfiguration configuration, IWebHostEnvironment webHostEnvironmentenv)
        {
            _productReadRepository = productReadRepository;
            this.configuration = configuration;
            _webHostEnvironmentenv = webHostEnvironmentenv;
        }

        public async Task<List<GetProductImagesQueryResponse>> Handle(GetProductImagesQueryRequest request, CancellationToken cancellationToken)
        {
            Domain.Entities.Product? product = await _productReadRepository.Table.Include(p => p.ProductImageFiles)
                   .FirstOrDefaultAsync(p => p.Id == Guid.Parse(request.Id));
            return product?.ProductImageFiles.Select(p => new GetProductImagesQueryResponse
            {
                Path = $"{_webHostEnvironmentenv.ContentRootPath}/{p.Path}",
                FileName = p.FileName,
                Id = p.Id
            }).ToList();
        }
    }
}
