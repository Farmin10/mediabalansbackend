﻿using MediaBalans.Application.Repositories.Category;
using MediaBalans.Application.Repositories.Product;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Application.Features.Queries.Category.GetAllCategory
{
    public class GetAllCategoryQueryHandler : IRequestHandler<GetAllCategoryQueryRequest, GetAllCategoryQueryResponse>
    {
        readonly ICategoryReadRepository _categoryReadRepository;
        readonly ILogger<GetAllCategoryQueryHandler> _logger;
        public GetAllCategoryQueryHandler(ICategoryReadRepository categoryReadRepository, ILogger<GetAllCategoryQueryHandler> logger)
        {
            _categoryReadRepository = categoryReadRepository;
            _logger = logger;
        }
        public async Task<GetAllCategoryQueryResponse> Handle(GetAllCategoryQueryRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Get all categories");

            var totalProductCount = _categoryReadRepository.GetAll(false).Count();

            var categories = _categoryReadRepository.GetAll(false).Skip(request.Page * request.Size).Take(request.Size)
                .Select(p => new
                {
                    p.Id,
                    p.CategoryName,
                }).ToList();

            return new()
            {
                Categories = categories,
                TotalCategoryCount = totalProductCount
            };
        }
    }
}
