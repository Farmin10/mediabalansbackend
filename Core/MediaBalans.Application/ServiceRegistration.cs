﻿using FluentValidation;
using MediaBalans.Application.Validators.Categories;
using MediaBalans.Application.Validators.Products;
using MediaBalans.Application.ViewModels.Categories;
using MediaBalans.Application.ViewModels.Products;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBalans.Application
{
    public static class ServiceRegistration
    {
        public static void AddApplicationServices(this IServiceCollection collection)
        {
            collection.AddMediatR(typeof(ServiceRegistration));
            collection.AddScoped<IValidator<VM_Create_Product>, CreateProductValidator>();
            collection.AddScoped<IValidator<VM_Create_Category>, CreateCategoryValidator>();
            collection.AddHttpClient();
        }
    }
}
