﻿namespace MediaBalans.Application.DTOs.User
{
    public class CreateUserResponse
    {
        public string Id { get; set; }
        public bool Succeeded { get; set; }
        public string Message { get; set; }
    }
}
